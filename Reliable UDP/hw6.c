#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "hw6.h"

#define TRUE 1
#define FALSE 0

#define MAX_TIMEOUT 4000

int sequence_number;
int expected_sn;

// To save the destination address
struct sockaddr_in dest_addr;
int header_size = sizeof(struct hw6_hdr);

// To estimate the RTT
#define ALPHA 0.7
#define BETA 0.8
#define INITIAL_RTT 400 //ms
#define INITIAL_DEV 50 //ms
int rtt;
struct timeval timeout;
float dev;

int timeval_to_msec(struct timeval *t) { 
	return t->tv_sec*1000+t->tv_usec/1000;
}

void msec_to_timeval(int millis, struct timeval *out_timeval) {
	out_timeval->tv_sec = millis/1000;
	out_timeval->tv_usec = (millis%1000)*1000;
}

int current_msec() {
	struct timeval t;
	gettimeofday(&t,0);
	return timeval_to_msec(&t);
}

int min(int a, int b){
	if(a <= b)
		return a;
	return b;
}

void set_rtt(int last_rtt){
	if(sequence_number != 0){
		dev = ALPHA * dev + (1 - ALPHA) * (abs(rtt-last_rtt));
		rtt = BETA * rtt + (1- BETA) * last_rtt;
	}
	else{
		rtt = last_rtt;
		dev = last_rtt / 2;
	}
	msec_to_timeval(rtt+4*dev,&timeout);
}

int rel_connect(int socket,struct sockaddr_in *toaddr,int addrsize) {
	dest_addr=*toaddr;
	return 0;
}

int rel_rtt(int socket) {
	return rtt;
}

void rel_send(int sock, void *buf, int len)
{
	// make the packet = header + buf
	char packet[MAX_PACKET];
	struct hw6_hdr *hdr = (struct hw6_hdr*)packet;
	memset(hdr,0,header_size);
	hdr->sequence_number = htonl(sequence_number);
	memcpy(hdr+1,buf,len); //hdr+1 is where the payload starts

	fprintf(stderr,"1) Sending packet %d: RTT= %dms, DEV= %.1fms, TIMEOUT= %dms\n",sequence_number,rtt,dev,timeval_to_msec(&timeout));
	sendto(sock,packet,header_size+len,0,(struct sockaddr*)&dest_addr,sizeof(dest_addr));

	int timeout_expired = FALSE;
	while(1){
		fd_set set;
		FD_ZERO(&set);
		FD_SET(sock,&set);
		struct timeval timeout_copy = timeout;

		int sent_begin = current_msec();
		int ready = select(FD_SETSIZE,&set,NULL,NULL,&timeout_copy);
		if(ready == -1)
			fprintf(stderr,"Select() failed\n"); 
		else if (ready == 0){
			msec_to_timeval(min(2 * timeval_to_msec(&timeout),MAX_TIMEOUT),&timeout);	
			fprintf(stderr,"---Sending packet %d: RTT= %dms, DEV= %.1fms, TIMEOUT= %dms\n",sequence_number,rtt,dev,timeval_to_msec(&timeout));
			sendto(sock,packet,header_size+len,0,(struct sockaddr*)&dest_addr,sizeof(dest_addr));
			timeout_expired = TRUE;
		}
			else{
				int received = 0;
				struct sockaddr_in source_addr;
				unsigned int addr_size = sizeof(source_addr);
				char recv_packet[MAX_PACKET];

				received = recvfrom(sock,recv_packet,MAX_PACKET,0,(struct sockaddr*)&source_addr,&addr_size);
				if(received < 0){
					fprintf(stderr,"Recvfrom failed\n");
					return;
				}
				struct hw6_hdr *hdr = (struct hw6_hdr*)recv_packet;
				if(ntohl(hdr->ack_number) == sequence_number){
					if(!timeout_expired)
						set_rtt(current_msec() - sent_begin);
					sequence_number++;
					break;				
				}
				if(! (hdr->type & ACK)){
					struct hw6_hdr ack_hdr;
					ack_hdr.type = ACK;
					if(ntohl(hdr->sequence_number) == expected_sn)
						expected_sn ++;			
					ack_hdr.ack_number = htonl(expected_sn-1);
					sendto(sock,&ack_hdr,sizeof(ack_hdr),0,(struct sockaddr*)&dest_addr,sizeof(dest_addr));
			}

		}
	}
}

int rel_socket(int domain, int type, int protocol) {
	rtt = INITIAL_RTT;
	dev = INITIAL_DEV;
	timeout.tv_sec = 0;
	timeout.tv_usec = (rtt + 4*dev)*1000;

	sequence_number = 0;
	expected_sn = 0;
	return socket(domain, type, protocol);
}

int rel_recv(int sock, void * buffer, size_t length) {
	char packet[MAX_PACKET];
	memset(&packet,0,sizeof(packet));
	struct hw6_hdr* hdr=(struct hw6_hdr*)packet;	

	while(1){
		unsigned int addrlen = sizeof(dest_addr);
		int received = recvfrom(sock,packet,MAX_PACKET,0,(struct sockaddr*)&dest_addr, &addrlen);
		//fprintf(stderr,"Received %d bytes\n",received);
		if(received < 0){
		//fprintf(stderr,"Error with recvfrom() in rel_recv\n");
			break;
		}
		//fprintf(stderr,"EXPECTED %d, REALD %d\n",expected_sn,hdr->sequence_number);
		if(ntohl(hdr->sequence_number) == expected_sn){
			struct hw6_hdr ack_hdr;
			ack_hdr.type = ACK;
			ack_hdr.ack_number = hdr->sequence_number;
		//fprintf(stderr,"1) Sending ack %d back\n",ack_hdr.ack_number);
			sendto(sock,&ack_hdr,sizeof(ack_hdr),0,(struct sockaddr*)&dest_addr,addrlen);
		//fprintf(stderr,"SENT\n");
			expected_sn++;
			memcpy(buffer,packet+header_size, received-header_size);
			return received - header_size;
		}
		else{
			struct hw6_hdr ack_hdr;
			ack_hdr.type = ACK;
			ack_hdr.ack_number = htonl(expected_sn-1);
		//fprintf(stderr,"2) Sending ack %d back\n",ack_hdr.ack_number);
			sendto(sock,&ack_hdr,sizeof(ack_hdr),0,(struct sockaddr*)&dest_addr,addrlen);
		}

	}
}

int rel_close(int sock) {
	rel_send(sock, 0, 0); // send an empty packet to signify end of file
	fprintf(stderr,"\nClosing\n");
	struct timeval t;
	t.tv_sec = 2;
	t.tv_usec = 0;
	setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO, &t, sizeof(t));
	int begin = current_msec();

	while(current_msec() - begin <2000)
		rel_recv(sock,0,0);
	close(sock);
}

