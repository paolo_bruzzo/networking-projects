#include <errno.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <dirent.h>

#define BACKLOG (10)
#define RECV_LEN 1024+13 //1024 is the url lenght, 13 is the rest of the GET command lenght
#define FILEPATH_LEN 1024
#define FILENAME_LEN 100
#define INDEX_HTML "index.html"
#define EXTENSION_SIZE 4
#define FILE_NUM 100

#define TRUE 1
#define FALSE 0

#define DIR_TYPE 'd'
#define FILE_TYPE 'f'

int request_parser(char *, char *, char *, char *); 
int send_file(int,char *, char *,char *);
void send_not_found(int);
void send_file_list(int,char list[][FILENAME_LEN+1],int,char*,char*);
void retrieve_folder(char*,char*);
/* Your program should take two arguments:
 * 1) The port number on which to bind and listen for connections, and
 * 2) The directory out of which to serve files.
 */

int main(int argc, char** argv) {
    /* For checking return values. */
    int retval;

    if(argc != 3){
        fprintf(stderr, "\nUsage: %s <port> <directory>\n\n", argv[0]);
        exit(1);
    }

    /* Read the port number from the first command line argument. */
    int port = atoi(argv[1]);

    /* Create a socket to which clients will connect. */
    int server_sock = socket(AF_INET6, SOCK_STREAM, 0);
    if(server_sock < 0) {
        perror("Creating socket failed");
        exit(1);
    }

    /* A server socket is bound to a port, which it will listen on for incoming
     * connections.  By default, when a bound socket is closed, the OS waits a
     * couple of minutes before allowing the port to be re-used.  This is
     * inconvenient when you're developing an application, since it means that
     * you have to wait a minute or two after you run to try things again, so
     * we can disable the wait time by setting a socket option called
     * SO_REUSEADDR, which tells the OS that we want to be able to immediately
     * re-bind to that same port. See the socket(7) man page ("man 7 socket")
     * and setsockopt(2) pages for more details about socket options. */
    int reuse_true = 1;
    retval = setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &reuse_true,
                        sizeof(reuse_true));
    if (retval < 0) {
        perror("Setting socket option failed");
        exit(1);
    }

    /* Create an address structure.  This is very similar to what we saw on the
     * client side, only this time, we're not telling the OS where to connect,
     * we're telling it to bind to a particular address and port to receive
     * incoming connections.  Like the client side, we must use htons() to put
     * the port number in network byte order.  When specifying the IP address,
     * we use a special constant, INADDR_ANY, which tells the OS to bind to all
     * of the system's addresses.  If your machine has multiple network
     * interfaces, and you only wanted to accept connections from one of them,
     * you could supply the address of the interface you wanted to use here. */
    
   
    struct sockaddr_in6 addr;   // internet socket address data structure
    addr.sin6_family = AF_INET6;
    addr.sin6_port = htons(port); // byte order is significant
    addr.sin6_addr = in6addr_any; // listen to all interfaces

    
    /* As its name implies, this system call asks the OS to bind the socket to
     * address and port specified above. */
    retval = bind(server_sock, (struct sockaddr*)&addr, sizeof(addr));
    if(retval < 0) {
        perror("Error binding to port");
        exit(1);
    }

    /* Now that we've bound to an address and port, we tell the OS that we're
     * ready to start listening for client connections.  This effectively
     * activates the server socket.  BACKLOG (#defined above) tells the OS how
     * much space to reserve for incoming connections that have not yet been
     * accepted. */
    retval = listen(server_sock, BACKLOG);
    if(retval < 0) {
        perror("Error listening for connections");
        exit(1);
    }

    /* Remember where the user is located*/
    char dir[FILEPATH_LEN+1];
  	strcpy(dir,argv[2]);

  	/* Loop */
    while(TRUE) {
    	//printf("==========\n");
        /* Declare a socket for the client connection. */
        int sock;
        char recv_buf[RECV_LEN+1];
        char filepath[FILEPATH_LEN+1];
        char filename[FILENAME_LEN+1];
        char extension[EXTENSION_SIZE+1];
        DIR *dirp;
        struct dirent *dp;
        struct stat stat_buf;

        memset(&filepath,0,sizeof(filepath));
        memset(&filename,0,sizeof(filename));
        memset(&extension,0,sizeof(extension));

        /* Another address structure.  This time, the system will automatically
         * fill it in, when we accept a connection, to tell us where the
         * connection came from. */
        struct sockaddr_in remote_addr;
        unsigned int socklen = sizeof(remote_addr); 

        /* Accept the first waiting connection from the server socket and
         * populate the address information.  The result (sock) is a socket
         * descriptor for the conversation with the newly connected client.  If
         * there are no pending connections in the back log, this function will
         * block indefinitely while waiting for a client connection to be made.
         * */
        sock = accept(server_sock, (struct sockaddr*) &remote_addr, &socklen);
        if(sock < 0) {
            perror("Error accepting connection");
            exit(1);
        }

        /* At this point, you have a connected socket (named sock) that you can
         * use to send() and recv(). */

        /* Receive the request by the client */
        recv(sock,recv_buf,RECV_LEN,0);
        /* Parse it */
        request_parser(recv_buf,filepath,filename,extension);

        /* path is the home directory (argv[2]+requested)*/
        char path[FILEPATH_LEN+1];
    	strcpy(path,argv[2]);


    	// if requested a directory, just take it
    	if(!strchr(filepath, '.'))
        	strcat(path,filepath);
        else
        	// remember the last directory
        	strcpy(path,dir);


        dirp = opendir(path);
        /* Open the directory passed as argument */
        if (dirp == NULL) {
            send_not_found(sock);
            continue;
        }

        /* True when the directory contains index.html */
        short found_index = FALSE;
        char file_store[FILE_NUM][FILENAME_LEN+1];
        char type[FILE_NUM];
        int i = 0;

        /* Read all the files and directories in the folder */
        while ((dp = readdir(dirp)) != NULL) {

            /* Save the entire path to read it correctly with stat() */
            char cur_file[FILEPATH_LEN];
            strcpy(cur_file,path);
           	strcat(cur_file,"/");
           	//Save the current directory globally
           	strcpy(dir,cur_file);
            strcat(cur_file,dp->d_name);

            //printf("Cur_file: %s\n",cur_file);
            /* Get entry's information. */
            if (stat(cur_file, &stat_buf) == -1){
                fprintf(stderr, "Error in reading %s\n", dp->d_name);
                continue;
            }

            strcpy(file_store[i],dp->d_name);
            if(S_ISDIR(stat_buf.st_mode))
            	type[i] = DIR_TYPE;
            else
            	type[i] = FILE_TYPE;
            i++;

            /* Set the variable to 'TRUE' if index.html is found */
            if(strcmp(INDEX_HTML,dp->d_name) == 0){
            	//printf("Found Index\n");
                found_index = TRUE;
            }
            //printf("File parsed: %s , index = %d\n",dp->d_name,found_index);

        }

     
        /* If the user asked for a directory without index.html, list files*/
        if(strcmp(filename,INDEX_HTML) == 0 && ! found_index){
            //printf("Index not found\n");
            //printf("----PATH: %s\n",filepath);
            send_file_list(sock,file_store,i,type,filepath);
        }
        else{
           char folder[FILEPATH_LEN+1];
           retrieve_folder(filepath,folder);
           if(strstr(path,folder)==NULL)
                strcat(path,filepath);
           if(send_file(sock,path,filename,extension) != 0)
                    send_not_found(sock);
        }
      	
        /* Tell the OS to clean up the resources associated with that client
         * connection, now that we're done with it. */
        close(sock);
    }

    close(server_sock);

    return 0;
}


/* Set the parameters of the header request*/
int request_parser(char recv_buf[], char filepath[], char filename[], char extension[]) {
    
    /* -------------------
     *  Get the filepath 
     * ------------------- */  
    short PATH_OFFSET = 4;   
    char *pch;
    pch = strchr(&recv_buf[PATH_OFFSET], ' ');
    strncpy(filepath, &recv_buf[PATH_OFFSET], pch-&recv_buf[PATH_OFFSET]);
    //printf("----Filepath: %s\n",filepath);

    /* -------------------
     *  Get the filename 
     * ------------------- */
    char *pch1 = filepath;
    /* Go to the last '/' */
    while (pch1 != NULL) {
        strcpy(filename, pch1 + 1);
        pch1 = strchr(pch1 + 1, '/');
    }

    /* If no file is specified, save "index.html" into filename*/
    if (!strchr(filename, '.'))
        strcpy(filename,INDEX_HTML);
    //printf("Filename: %s\n", filename);

    /* -------------------
     *  Get the file extension 
     * ------------------- */
     strcpy(extension,strchr(filename,'.')+1);
     //printf("Extension %s\n",extension);

    return 0;
}

/* Returns:
    0: in case of success
    1: in case of error while opening the file
    2: in case of memory allocation failure on the server
 */
int send_file(int sock, char filepath[], char filename[], char extension[]){
    
    FILE *fp;
    char *content;
    size_t result;
    long lSize;
    char complete_filepath[FILEPATH_LEN];

    short last = strlen(filepath);
    if(filepath[last-1] != '/' && !strchr(filepath, '.'))
    	strcat(filepath,"/");

    /* Save the filepath */
    strcpy(complete_filepath,filepath);
    if (!strchr(complete_filepath, '.'))
        strcat(complete_filepath,filename);

    /* Open the file */
    fp = fopen(complete_filepath,"rb");
    if(fp == NULL){
        fprintf(stderr, "Error while opening the file %s\n",complete_filepath);
        return 1;
    }

    fseek(fp,0,SEEK_END);
    lSize = ftell(fp);
    rewind(fp);

    content = (char*) malloc(sizeof(char)*lSize);
    if(content == NULL){
        fprintf(stderr, "Error while allocating memory\n");
        return 2;
    }

    result = fread(content, 1, lSize, fp);
    if(result != lSize){
        fprintf(stderr, "Error while reading %s\n",filename);
    }

    char content_type[50];

    /* Set the correct content type */
    if(strcmp(extension,"html") == 0) 
        strcpy(content_type,"text/html; charset=utf-8");
    if(strcmp(extension,"ico") == 0) 
        strcpy(content_type,"image/x-icon");
    if(strcmp(extension,"jpg") == 0) 
        strcpy(content_type,"image/jpeg");
    if(strcmp(extension,"png") == 0) 
        strcpy(content_type,"image/png");
    if(strcmp(extension,"gif") == 0) 
        strcpy(content_type,"image/gif");
    if(strcmp(extension,"pdf") == 0) 
        strcpy(content_type,"application/pdf");
    
    /* Save everything in a buffer */
    char response[lSize+301];
    sprintf(response,"HTTP/1.0 200 OK\r\nServer: SimpleHTTP/0.6\r\nContent-type: %s\r\nContent-Length: %zu\r\n\r\n",content_type,result);
    int response_size = strlen(response) + lSize;
    memcpy(response+strlen(response),content,lSize);

    /* To keep in memory the number of bytes sent*/
    int total_sent = 0;
    int remaining = response_size;
    int sent;

    while(total_sent < response_size){
        sent = send(sock, response+total_sent, remaining , 0);
        if(sent == -1) break;
        total_sent += sent;
        remaining -= sent;
    }
    
    /* Print the feedback */
    if(total_sent != response_size)
        fprintf(stderr, "Error while sending %s\n",filename);
    else
        printf("OK: %s has been sent (%d bytes)\n",filename,response_size);

    free(content);
    fclose(fp);
    return 0;
}


void send_not_found(int sock){
    char response[601];
    sprintf(response,"HTTP/1.0 404 File not found\r\nServer: SimpleHTTP/0.6\r\nContent-Type: text/html\r\n\r\n<html><head><title>Error Page</title></head><body><h1>Error response</h1><p>Error code 404: File Not Found</p><p>Don't worry, you are not here for anything! We give you the pinacolada recipe:</p><ul><li>2 oz of light rum</li><li>3 oz of pineapple juice</li><li>2 oz of coconut cream</li><li>Ice</li></ul></body></html>");
    /* Send it */

    int total_sent = 0;
    int response_size = strlen(response);
    int remaining = response_size;
    int sent;

    while(total_sent < response_size){
        sent = send(sock, response + total_sent, remaining , 0);
        if(sent == -1) break;
        total_sent += sent;
        remaining -= sent;
    }
    
    /* Print the feedback */
    if(total_sent != strlen(response))
        fprintf(stderr, "Error while sending\n");
    else
        printf("FNF: Error Page has been sent (%d bytes)\n",sent);
    close(sock);
}

void send_file_list(int sock, char list[][FILENAME_LEN+1], int filenum, char types[],char path[]){
	int i;
	char response[10000];
	char link[100];

    sprintf(response,"HTTP/1.0 200 OK\r\nServer: SimpleHTTP/0.6\r\nContent-Type: text/html\r\n\r\n<html><title>Directory listing for /</title><body><h2>Directory listing for /</h2><hr><ul>");
    
	for(i=0; i< filenum ; i++){
        //don't show hidden files
        if(list[i][0]!='.'){
    		char current_dir[FILEPATH_LEN+1];
    		
    		memset(&link,0,sizeof(link));
    		memset(&current_dir,0,sizeof(current_dir));
    		strcpy(current_dir,path);
    		strcat(current_dir,"/");

            /*Check if the requested file is a directory or a file*/
    		switch(types[i]){
    			case FILE_TYPE: 
    					sprintf(link,"<li><a href=\"%s\">%s</a>",list[i],list[i]); 
    				break;
    			case DIR_TYPE:{ 
    					strcat(current_dir,list[i]);
    					sprintf(link,"<li><a href=\"%s\">%s</a>",current_dir,list[i]); 
    				}
    				break;
     		}
    		strcat(response,link);
        }
    }

	strcat(response,"</ul><hr></body></html>");

    //SEND
    int total_sent = 0;
    int response_size = strlen(response);
    int remaining = response_size;
    int sent;

    while(total_sent < response_size){
        sent = send(sock, response + total_sent, remaining, 0);
        if(sent == -1) break;
        total_sent += sent;
        remaining -= sent;
    }

    
    /* Print the feedback */
    if(total_sent != response_size)
        fprintf(stderr, "Error while sending\n");
    else
        printf("OK: File List Page has been sent (%d bytes)\n",sent);

}

void retrieve_folder(char filepath[], char folder[]){

    int i;
    int place;
    for(i=0;i<strlen(filepath);i++){
        if(filepath[i]=='/')
            place=i;
    }
    strncpy(folder,filepath,place+1);
    folder[place+1]='\0';
    //printf("====FILEPATH: %s\n",filepath);
    //printf("====FOLDER: %s\n",folder);
}
