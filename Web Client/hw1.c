/* 
 * File:   hw1.c
 * Author: paolobruzzo
 *
 * Created on August 31, 2014, 9:59 AM
 * 
 * This script takes a URL as only input, and 
 * downloads the page/file directly associated to it
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define URL_LEN 200
#define FILEPATH_LEN 170
#define FILENAME_LEN 150
#define REQ_LEN 300
#define RES_LEN 1000
#define HTTP_PORT "80"
#define DEFAULT_FILEPATH "/index.html"
#define DEFAULT_FILENAME "index.html"
#define RESP_OFFSET 9

#define TRUE 1
#define FALSE 0

FILE *fp;

void url_parser(char *url, char *domain, char *filepath, char *filename);
int find_blanck_line(char *buffer, int len);

int main(int argc, char** argv) {

    struct addrinfo hints;
    struct addrinfo * result, * rp;
    int sock_fd, s;
    char domain[URL_LEN + 1];
    char filepath[FILEPATH_LEN + 1];
    char filename[FILENAME_LEN + 1];

    /* Exit if the number of parameters is wrong */
    if (argc != 2) {
        fprintf(stderr, "The script wants a URL as an argument\n");
        return 1;
    }

    /* Init hints */
    memset(&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    /* Parse URL */
    printf("\n----------------------URL INFO-----------------------\n");
    url_parser(argv[1], domain, filepath, filename);
    printf("------------------------------------------------------\n");

    /* Init address struct */
    s = getaddrinfo(domain, HTTP_PORT, &hints, &result);
    if (0 != s) {
        fprintf(stderr, "DNS lookup failed, this domain may not exist\n\n");
        exit(1);
    }

    /* Iterate on the found IPs */
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sock_fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sock_fd == -1)
            continue;

        /* Try to connect */
        if (connect(sock_fd, rp->ai_addr, rp->ai_addrlen) != -1)
            break;

        /* If does not succeed, close socket */
        close(sock_fd);
    }

    /* If DNS lookup fails, print it */
    if (rp == NULL) {
        fprintf(stderr, "Could not connect to: %s\n", domain);
        exit(1);
    }

    /* Otherwise... */
    printf("Connected to: %s\n", domain);
    freeaddrinfo(result);

    char send_buf[REQ_LEN + 1];
    char recv_buf[RES_LEN + 1];
    int bytes_sent;

    memset(&send_buf, 0, sizeof (send_buf));
    memset(&recv_buf, 0, sizeof (recv_buf));

    /* Send request */
    sprintf(send_buf, "GET %s HTTP/1.0\r\nHost: %s\r\nUser-Agent: HTMLGET 1.0\r\n\r\n", filepath, domain);
    printf("Sending request ...\n%s", send_buf);
    bytes_sent = send(sock_fd, send_buf, strlen(send_buf), 0);

    /* Check if the request is successful */
    if (bytes_sent == -1 || bytes_sent != strlen(send_buf)) {
        fprintf(stderr, "Error while sending the request\n");
        exit(1);
    }

    /* Receive response */
    int recv_count = recv(sock_fd, recv_buf, RES_LEN, 0);
    if (recv_count < 0) {
        perror("Receive failed");
        exit(1);
    }

    printf("------------------------------------------------------\n");

    /* Check the response code */
    switch (recv_buf[RESP_OFFSET]) {
        case '2': printf("OK Page found\nDownloading...\n");
            break;
        case '3': fprintf(stderr, "Redirection not supported\n\n");
            exit(3);
        case '4': fprintf(stderr, "Page not found\n\n");
            exit(4);
        case '5': fprintf(stderr, "Server error\n\n");
            exit(5);
        default: fprintf(stderr, "Ooops something went wrong\n\n");
            exit(5);
    }

    /* Pointer to the end of the response header */
    int head_bytes = -1;
    short found = FALSE;

    /* Open the file where to write */
    fp = fopen(filename, "wb");
    if (fp == NULL) {
        fprintf(stderr, "Error while writing the file\n");
        exit(1);
    }

    /* Keep reading the stream from the socket */
    while (recv_count > 0) {

        /* Find the end of the header */
        head_bytes = find_blanck_line(recv_buf, RES_LEN);

        if (head_bytes != -1 && !found) {
            found = TRUE;

            // Overwrite recv_buff. I use a support array to avoid abort trap
            char recv_supp[RES_LEN];
            memcpy(recv_supp, &recv_buf[head_bytes], RES_LEN - head_bytes);
            memcpy(recv_buf, recv_supp, RES_LEN);

            // Remove the header bytes
            recv_count = recv_count - head_bytes;
        }

        /* Write on file */
        if (found)
            fwrite(recv_buf, recv_count, 1, fp);

        /* Reset the buffer and read again */
        memset(&recv_buf, 0, sizeof (recv_buf));
        recv_count = recv(sock_fd, recv_buf, RES_LEN, 0);
    }

    /* Close file */
    fclose(fp);

    /* Close socket */
    printf("Closing socket\n");
    shutdown(sock_fd, SHUT_RDWR);
    printf("Done\n\n");
    printf("------------------------------------------------------\n");
    return 0;
}

/* ================================
  FUNCTIONS 
=================================== */

/* Set the parameters */
void url_parser(char url[], char domain[], char filepath[], char filename[]) {

    /* Check the lenght of the URL*/
    if (strlen(url) > URL_LEN) {
        fprintf(stderr, "URL too long\n");
        exit(1);
    }

    /* If the url starts with http, just remove it */
    if (url[0] == 'h' || url[0] == 'H'){
        char url_supp[URL_LEN];
	memcpy(url_supp, url + 7, URL_LEN - 7);
    	memcpy(url,url_supp,URL_LEN);
    }

    /* --------------------------------
    Extract the domain from the URL
    ----------------------------------- */
    char *pch;
    pch = strchr(url, '/');

    if (pch != NULL)
        strncpy(domain, url, pch - url);
    else
        strcpy(domain, url);

    printf("Domain to connect to: http://%s\n", domain);

    /* --------------------------------
     Extract the filepath from the URL
     ----------------------------------- */
    memcpy(filepath, url + strlen(domain), FILEPATH_LEN - strlen(domain));

    if (strlen(filepath) == 0 || strlen(filepath) == 1)
        strcpy(filepath, DEFAULT_FILEPATH);

    printf("Filepath: %s\n", filepath);

    /* --------------------------------
    Extract the filename from the URL
    ----------------------------------- */

    /* Go to the last '/' */
    while (pch != NULL) {
        strcpy(filename, pch + 1);
        pch = strchr(pch + 1, '/');
    }

    /* If there is no file type, save the html page */
    if (!strchr(filename, '.'))
        strcpy(filename, DEFAULT_FILENAME);

    printf("Filename: %s\n", filename);
}

/* Return the number of header bytes */
int find_blanck_line(char buffer[], int buffer_len) {
    int i;
    short found = FALSE;

    for (i = 0; i < buffer_len - 4 && !found; i++) {
        if (buffer[i] == '\r')
            if (buffer[i + 1] == '\n')
                if (buffer[i + 2] == '\r')
                    if (buffer[i + 3] == '\n') {
                        found = TRUE;
                        return i + 4;
                    }
    }

    return -1;

}
