#!/usr/bin/python

__author__ = 'paolobruzzo'

import random
import socket
import struct
import sys

QID = 40000

found = {
    "A": False,
    "AAAA": False,
    "MX": False,
    "CNAME": False
}

# useful result codes
rcodes = {
    0: "No Error",
    1: "Format Error",
    2: "Server Failure",
    3: "Non-Existent Domain",
    4: "Not Implemented",
    5: "Query Refused"
}

# useful resource types
rtypes = {
    1: "A",
    2: "NS",
    5: "CNAME",
    12: "PTR",
    15: "MX",
    28: "AAAA"
}

rtypes_rev = {
    "A": 1,
    "NS": 2,
    "CNAME": 5,
    "PTR": 12,
    "MX": 15,
    "AAAA": 28
}

# efficiently print results to the screen like `host` does
output_format_strings = {
    "A": "%s has address %s",
    "AAAA": "%s has IPv6 address %s",
    "MX": "%s mail is handled by %s",
    "CNAME": "%s is an alias for %s",
    "PTR": "%s domain name pointer %s"
}

# list of bad servers
bad_servers = []

def debug(s):
    #sys.stderr.write(s + "\n")
    dont_bother_me_with_debugging = True


def set_bits(data, value, offset, numbits=1):
    mask = ((1 << numbits) - 1) << offset
    clear = 0xffff ^ mask
    data = (data & clear) | ((value << offset) & mask)
    # print "EDITED DATA: "+ str(binascii.hexlify(data))
    return data


def get_bits(data, offset, numbits=1):
    mask = ((1 << numbits) - 1) << offset
    return (data & mask) >> offset


def read_root_nameservers():
    ipv4_roots = []
    ipv6_roots = []
    with open('named.root') as f:
        for line in f.readlines():
            if line.find(';') == 0:
                continue
            elements = line.split()
            if elements[2] == 'A':
                debug("found IPv4 root nameserver: %s" % elements[3])
                ipv4_roots.append(elements)
            elif elements[2] == 'AAAA':
                debug("found IPv6 root nameserver: %s" % elements[3])
                ipv6_roots.append(elements)
    return ipv4_roots, ipv6_roots


def encode_hostname(hostname):
    output = ''
    elements = hostname.split('.')
    for item in elements:
        output += struct.pack("!B", len(item))
        output += item
    output += b'\0'
    return output


def decode_hostname(packet, ptr):
    # packet is byte array, ptr is offset into byte array - need both
    # to understand pointers for dns name compression

    name = []
    retptr = None
    while True:
        ptr_or_length = struct.unpack("!B", packet[ptr])[0]
        if ptr_or_length >= 192:
            if retptr is None:
                retptr = ptr + 2
            # ptr_or_length is a pointer - mask off 11, start interpreting at new ptr
            ptr = get_bits(struct.unpack("!H", packet[ptr:ptr + 2])[0], 0, 14)
            continue
        else:
            # ptr_or_length is a length - if 0 then done, else emit new hostname part
            if ptr_or_length == 0:
                ptr += 1  # consume the \0
                break  # done
            ptr += 1
            name.append(packet[ptr:ptr + ptr_or_length])
            ptr += ptr_or_length
    name = '.'.join(name)
    debug("parsed name: " + name)
    if retptr:
        ptr = retptr
    return name, ptr


def create_query(qtype, qname):
    hostname = encode_hostname(qname)
    qtype = struct.pack("!H",rtypes_rev[qtype])
    qclass = struct.pack("!H", 1)  # hard coded "internet address" - ok to leave this alone
    return hostname + qtype + qclass


def create_packet(qtype, qname):
    # query id
    qid = QID

    # flags, infos at: http://www.tcpipguide.com/free/t_DNSMessageHeaderandQuestionSectionFormat.htm
    qr = 0  # 0 when asking for a query, 1 when server is responding
    opcode = 0  # 0 for normal look up, 1 for inverse lookup ...2,3,4,5 look on the website
    aa = 0  # 0 if the response is from a NON authoritative server, 1 otherwise
    tc = 0  # 0 if the message hasn't been truncated, 1 otherwise
    rd = 0  # 0 if the recursion in NOT desired, 1 otherwise
    ra = 0  # 0 if the answering server does not support recursion, 1 otherwise
    z = 0  # reserved bits
    rcode = 0  # set to 0 in the query, changed by the server: look at the website

    # counts
    qdcount = 1  # number of questions
    anscount = 0  # number of resources records in the ANSWER
    authcount = 0  # number of resources records in the AUTHORITATIVE
    addlcount = 0  # number of resources records in the ADDITIONAL

    # pack flags into two bytes
    flagbits = 0b0000000000000000

    # set 8th most significant bit to true for recursion desired -
    if rd == 1:
        set_bits(flagbits, 8, 1)

    # ! is for network byte order - important!
    dns_header = struct.pack("!HHHHHH", qid, flagbits, qdcount, anscount, authcount, addlcount)
    dns_query = create_query(qtype, qname)

    return dns_header + dns_query


def parse_response(packet):
    # returns tuple of success + success struct, or failure + failure reason

    header_bytes = packet[:12]
    qid, flagbits, qdcount, anscount, authcount, addlcount = struct.unpack("!HHHHHH", header_bytes)

    if qid != QID:
        print "Received wrong query id (%d) should be %d" % (qid, QID)
        exit(1)

    rcode = get_bits(flagbits, 0, 4)
    if rcode not in rcodes:
        debug("unrecognized rcode: %d" % rcode)
    else:
        debug("rcode %d, meaning: %s" % (rcode, rcodes[rcode]))
    if rcode != 0:
        print "Failed: " + rcodes[rcode]
        sys.exit(1)

    # parse over question
    qoffset = 0
    ptr = 12
    while qoffset < qdcount:
        dontcare, ptr = decode_hostname(packet, ptr)
        debug("this was a query for %s" % dontcare)
        ptr += 4
        qoffset += 1

    total_results = anscount + authcount + addlcount
    debug("found %d results total (%d ans, %d auth, %d addl)." % (total_results, anscount, authcount, addlcount))
    answer_offset = 0
    #used to save the answers
    final_answers = []
    #used to save the 'A' and 'AAAA' servers that are not in answers
    other_servers = []

    while answer_offset < total_results:
        debug("resource found at ptr %d" % ptr)
        answer_name, ptr = decode_hostname(packet, ptr)
        debug("new ptr is %d" % ptr)
        ans_type, ans_class, ttl, rdlen = struct.unpack("!HHIH", packet[ptr:ptr + 10])
        ptr += 10
        rdata = packet[ptr:ptr + rdlen]
        ptr += rdlen
        debug("found resource for name %s" % answer_name)
        if ans_type in rtypes:
            #ipv4 addresses
            if rtypes[ans_type] == "A":
                # if this is in answer section, we have a final answer!
                resource = socket.inet_ntop(socket.AF_INET, rdata)
                debug("found ipv4 address %s" % resource)
                if answer_offset < anscount:
                    final_answers.append(("A", answer_name, resource))
                else:
                    other_servers.append(("A", answer_name, resource))
            #ipv6 addresses
            elif rtypes[ans_type] == "AAAA":
                resource = socket.inet_ntop(socket.AF_INET6, rdata)
                debug("found ipv6 address %s" % resource)
                if answer_offset < anscount:
                    final_answers.append(("AAAA", answer_name, resource))
                else:
                    other_servers.append(("AAAA", answer_name, resource))
            #MX addresses
            elif rtypes[ans_type] == "MX":
                ptr -= rdlen - 2
                resource, ptr = decode_hostname(packet, ptr)
                debug("found MX address %s" % resource)
                if answer_offset < anscount:
                    final_answers.append(("MX", answer_name, resource))
            #CNAME addresses
            elif rtypes[ans_type] == "CNAME":
                ptr -= rdlen
                resource, ptr = decode_hostname(packet, ptr)
                debug("found CNAME address %s" % resource)
                if answer_offset < anscount:
                    final_answers.append(("CNAME", answer_name, resource))
            else:
                debug("found %s record" % rtypes[ans_type])
        else:
            debug("found rtype %d, don't care for this assignment" % ans_type)
        answer_offset += 1
    return final_answers, other_servers


def usage():
    print "usage: python %s www.example.com" % sys.argv[0]


def print_servers(servers, prefix=""):
    for item in servers:
        rectype, name, value = item
        if rectype in output_format_strings and not found[rectype]:
            print prefix + output_format_strings[rectype] % (name, value)

def add_bad_server(server):
    global bad_servers
    bad_servers.append(server)

def reset_bad_servers():
    global bad_servers
    bad_servers = []

def set_global_found(key,value):
    global found
    found[key] = value

def reset_global_found():
    for item in found:
        set_global_found(item,False)

def edit_founds(servers):
    for item in servers:
        rectype, name, value = item
        set_global_found(rectype, True)

# recoursive query
def send_query(querybytes, famtype="IPv4", dns_server="198.41.0.4", port=53, recursion_cont=0):
    if famtype == "IPv4":
        sock_fam = socket.AF_INET
    elif famtype == "IPv6":
        sock_fam = socket.AF_INET6

    # Used to print a tree like tree-structure
    prefix = "Lev " + str(recursion_cont) + ": " + "    " * recursion_cont

    try:
        sock = socket.socket(sock_fam, socket.SOCK_DGRAM)
        sock.settimeout(0.3)
        sock.sendto(querybytes, (dns_server, port))
        data, addr = sock.recvfrom(1500)
    except Exception:
        #print "...Server %s not responding" % dns_server
        add_bad_server(dns_server)
        return False

    final_answers, other_servers = parse_response(data)

    # Autorecursive server -> BAD
    if recursion_cont > 5:
        add_bad_server(dns_server)
        return False

    if len(final_answers) == 0:
        #print_servers(other_servers, prefix + "Other Server: ")

        for elem in other_servers:
            i = other_servers.index(elem)
            if elem[0] == "A" and other_servers[i][2] not in bad_servers:
                #print prefix + "Querying IPv4 server: ", other_servers[i][2]
                found1 = send_query(querybytes, "IPv4", other_servers[i][2], port, recursion_cont + 1)
                if found1:
                    return True
            elif elem[0] == "AAAA" and other_servers[i][2] not in bad_servers:
                #print prefix + "Querying IPv6 server: ", other_servers[i][2]
                found1 = send_query(querybytes, "IPv6", other_servers[i][2], port, recursion_cont + 1)
                if found1:
                    return True

        if recursion_cont == 0:
            return False
    else:
        print_servers(final_answers)
        for item in final_answers:
            if "CNAME" in item and not found["CNAME"]:
                rectype, name, server_alias = item
                print "Restarting with CNAME...%s" % server_alias
                target(server_alias)
                break
        edit_founds(final_answers)
        return True


def target(target_hostname):
    #print "Querying starting form %s" %target_hostname
    reset_global_found()
    reset_bad_servers()

    querybytes1 = create_packet('A', target_hostname)
    querybytes2 = create_packet('AAAA', target_hostname)
    querybytes3 = create_packet('MX', target_hostname)

    ipv4_roots, ipv6_roots = read_root_nameservers()
    random.shuffle(ipv4_roots)
    random.shuffle(ipv6_roots)

    #Limit the lookup to 2 IPv4 servers
    for elem in ipv4_roots[:2]:
        if not found["A"]:
            send_query(querybytes1, "IPv4", elem[3])
        if not found["AAAA"]:
            send_query(querybytes2, "IPv4", elem[3])
        if not found["MX"]:
            send_query(querybytes3, "IPv4", elem[3])

    #Limit the lookup to 2 IPv6 servers
    for elem in ipv6_roots[:2]:
        if not found["A"]:
            send_query(querybytes1, "IPv6", elem[3])
        if not found["AAAA"]:
            send_query(querybytes2, "IPv6", elem[3])
        if not found["MX"]:
            send_query(querybytes3, "IPv6", elem[3])


def main():
    if len(sys.argv) != 2:
        usage()
        sys.exit(1)

    #target("www.dropbox.com")
    target(sys.argv[1])

if __name__ == "__main__":
    main()
